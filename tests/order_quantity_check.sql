with source as(
    select * from {{ref('fact_customer_order')}}
)
select quantity from source
where quantity < 1