with source as(
    select * from {{ref('dim_customer')}}
)
select age from source
where age < 18 AND  (FLOOR(age) = age)