with source as(

    select * from {{ref('stg_ev_customer')}}
),
source1 as (

    select * from {{ref('dim_customer')}}
)

Select *
From source A
Where NOT EXISTS
( select * from source1 B
Where A.cust_id = B.cust_id
and A.gender = B.gender
and A.dor = B.dor
and A.dob=B.dob
)