{{
    config(
        materialized='incremental'
    )
}}

select row_number() over(partition by 1 order by ev_launch_date) 
{% if is_incremental() %}
+
(select max(ev_model_dimension_id) from {{this}})
{% endif %}
as ev_model_dimension_id, 
length,
width,
height,
v_weight,
ev_id
from stg_ev_model
