{{
    config(
        materialized='incremental'
    )
}}

with source as (
 
    select * from {{ref('stg_ev_model') }}
 
),

source1 as(
    select * from {{ref('dim_ev_model_battery_specs')}}
),

source2 as(
    select * from {{ref('dim_ev_model_dimension')}}
)

select distinct row_number() over(partition by 1 order by ev_launch_date)
{% if is_incremental() %}
+
(select max(ev_model_specs_id) from {{this}})
{% endif %} ev_model_specs_id,
acceleration,
torque,
v_range,
speed,
x.ev_model_battery_specs_id,
x1.ev_model_dimension_id
from source b
inner join
source1 x
on x.Battery_Capacity=b.Battery_Capacity
inner join 
source2 x1
on x1.ev_id = b.ev_id


