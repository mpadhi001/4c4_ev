 {{
    config(
        materialized='incremental'
    )
}}
with source as (
 
    select * from {{ref('stg_user') }}
 
),
source1 as(
    select * from {{ref('dim_city')}}
),
source2 as(
    select * from {{ref('dim_discover')}}
),
source3 as(
    select * from {{ref('dim_dealership')}}
)
select cust_id,
u_name,
u_gender,
u_email,
u_phone_no,
u_dor,
u_dob,
age,
b.customer_city_id,
c.customer_discover_id,
dealership_id from source a
inner join source1 b 
on a.u_city=b.city
inner join source2 c 
on a.how_they_found_us=c.how_they_found_us
