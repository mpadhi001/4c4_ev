{{
    config(
        materialized='incremental'
    )
}}
with source1 as(
    select distinct how_they_found_us from {{ref('stg_ev_customer')}}
)

select distinct row_number() over(partition by 1 order by how_they_found_us) 
{% if is_incremental() %}
+
(select max(customer_discover_id) from {{this}})
{% endif %}
customer_discover_id,
how_they_found_us
from source1
