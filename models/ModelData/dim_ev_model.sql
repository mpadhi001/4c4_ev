{{
    config(
        materialized='table'
    )
}}

with source as (
 
    select * from {{ref('stg_ev_model') }}
 
),

source1 as(
    select * from {{ref('dim_ev_model_specs') }}
 
)

select model_no,ev_id,
model_name,
ev_launch_date,
price,
x.ev_model_specs_id
from source b 
inner join source1 as x 
on x.acceleration=b.acceleration