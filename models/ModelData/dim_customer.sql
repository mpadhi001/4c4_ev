{{
    config(
        materialized='incremental'
    )
}}
with source as (
 
    select * from {{ref('stg_ev_customer') }}
 
),
source1 as(
    select * from {{ref('dim_city')}}
),
source2 as(
    select * from {{ref('dim_discover')}}
)

select cust_id,
cust_name,
gender,
email,
phone_no,
dor,
dob,
age,
b.customer_city_id,
c.customer_discover_id from source a
inner join source1 b 
on a.city=b.city
inner join source2 c 
on a.how_they_found_us=c.how_they_found_us
