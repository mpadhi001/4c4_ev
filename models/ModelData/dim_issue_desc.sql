{{
    config(
        materialized='table'
    )
}}

with source as(

    select * from {{ref('dim_customer_issue')}}
)
select issue_desc_id,issue from source