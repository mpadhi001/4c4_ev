{{
    config(
        materialized='incremental'
    )
}}
with source as (
 
    select  * from {{ref('stg_dealership') }}
 
),
source1 as(
    select  * from {{ref('dim_city')}}
)


select d_name,
d_id,
b.customer_city_id
from source a
inner join source1 b
on b.city=a.d_city

