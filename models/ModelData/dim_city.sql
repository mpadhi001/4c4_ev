{{
    config(
        materialized='incremental'
    )
}}

with source as(
    select distinct city,cust_state from {{ref('stg_ev_customer')}}
),

source1 as(

    select * from {{ref('dim_state')}}
)

select distinct row_number() over(partition by 1 order by city) 
{% if is_incremental() %}
+
(select max(customer_city_id) from {{this}})
{% endif %}
customer_city_id,
city,
b.customer_state_id from source as a
inner join source1 b on a.cust_state=b.cust_state