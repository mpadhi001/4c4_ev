{{
    config(
        materialized='incremental'
    )
}}

with source as(
    select distinct cust_state from {{ref('stg_ev_customer')}}
)


select distinct row_number() over(partition by 1 order by cust_state) 
{% if is_incremental() %}
+
(select max(customer_state_id) from {{this}})
{% endif %}
customer_state_id,
cust_state 
from source