{{
    config(
        materialized='incremental'
    )
}}
with source as (
 
    select * from {{ref('stg_salesforce_customer') }}
 
)

select row_number() over(partition by 1 order by create_date) 
{% if is_incremental() %}
+
(select max(issue_desc_id) from {{this}})
{% endif %}
as issue_desc_id,* from source