
{{
    config(
        materialized='incremental'
    )
}}
with source as (
 
    select * from {{ref('stg_ev_model') }}
 
)
select row_number() over(partition by 1 order by ev_launch_date) 
{% if is_incremental() %}
+
(select max(ev_model_battery_specs_id) from {{this}})
{% endif %}
as ev_model_battery_specs_id,
battery_type,
battery_origin,
battery_warranty,
charge_capacity,
fast_charging,
battery_capacity from stg_ev_model

