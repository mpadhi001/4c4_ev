{{
    config(
        materialized='table'
    )
}}
with source as (
 
    select * from {{ref('stg_salesforce_feedback') }}
 
)

select * from source