with source as (
 
    select * from {{source('Snowflake_Landing','SALESFORCE_FEEDBACK__C') }}
 
), 
 
salesforce_feedback_renamed as (
 
select 
 
    "ID" as cust_id,	
    "NAME" as cust_name,	
    "CREATEDDATE" as create_date,	
    "CREATEDBYID" as created_by_id,
    "CUSTOMER_NAME__C" as salesforce_cust_name,	
    "ISSUE_RAISED_DATE__C" as issue_raised_date,	
    "ISSUE__C" as issue,	
    "MODEL_NUMBER__C" as model_no,
    "ORDERID__C" as order_id,
    "ISSUE_RESOLVED__C" as issue_resolved,	
    "FEEDBACK__C" as feedback

 
from source 
 
) 
 
select * from salesforce_feedback_renamed