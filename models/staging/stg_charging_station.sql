with source as (
 
    select * from {{source('Snowflake_Landing','CHARGING_INGEST') }}
 
), 
 
charging_station_renamed as (
 
select 
 
    "TITLE" as title,
    "CATEGORY" as category,
    "ADDRESS" as station_address,
    "CITY" as station_city,
    "ZIP" as station_zip,
    "COUNTRY" as station_country

 
from source 
 
) 
 
select * from charging_station_renamed