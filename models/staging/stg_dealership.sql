with source as (
 
    select * from {{source('Snowflake_Landing','RDS_DEALERSHIP') }}
 
), 
 
dealership_renamed as (
 
select 
 
    "DEALERSHIP_NAME" as d_name,
    "DEALERSHIP_ID" as d_id,
    "DEALERSHIP_CITY" as d_city,
    "DEALERSHIP_STATE" as d_state

 
from source 
 
) 
 
select * from dealership_renamed