with source as (
 
    select * from {{source('Snowflake_Landing','RDS_CUSTOMER') }}
 
), 
 
cust_renamed as (
 
select 
 
    "NAME" as cust_name,
    "PHONE_NO" as phone_no,
    "CITY" as city,
    "DOR" as dor,
    "DOB" as dob,
    "STATE" as cust_state,
    "EMAIL" as email,
    "GENDER" as gender,
    "CUSTOMER_ID" as cust_id,
    "AGE_IN_YEARS_" as age,
    "HOW_THEY_FOUND_US_" as how_they_found_us

 
from source 
 
) 
 
select * from cust_renamed