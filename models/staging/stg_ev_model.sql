with source as (
 
    select * from {{source('Snowflake_Landing','RDS_EV_MODEL') }}
 
), 
 
renamed as (
 
select 
 
    "EV_LAUNCH_DATE" as ev_launch_date,
    "BATTERY_TYPE" as battery_type,
    "BATTERY_ORIGIN" as battery_origin,
    "WEIGHT_OF_THE_VEHICLE__IN_KG_" as v_weight,
    "FAST_CHARGING" as fast_charging,
    "BATTERY_WARRANTY__IN_YRS_" as battery_warranty,
    "EV_MODEL_NUMBER" as model_no,
    "ACCELERATION__0_40___IN_SEC_" as acceleration,
    "OVERALL_WIDTH__IN_MM_" as width,
    "CHARGER_TYPE_CAPACITY" as charge_capacity,
    "OVERALL_HEIGHT__IN_MM_" as height,
    "EV_MODEL_NAME" as model_name,
    "EV_ID" as ev_id,
    "TORQUE_AT_MOTOR_SHAFT__IN_NM_" as torque,
    "VEHICLE_BATTERY_CAPACITY__IN_GWH_" as battery_capacity,
    "OVERALL_LENGTH__IN_MM_" as length,
    "EV_MODEL_PRICE__IN_RUPEES_" as price,
    "TOP_SPEED__IN_KM_HR_" as speed,
    "RANGE_OF_VEHICLE__IN_KM_" as v_range
 
from source 
 
) 
 
select * from renamed