with source as (
 
    select * from {{source('Snowflake_Landing','RDS_USERNEW') }}
 
), 
 
user_renamed as (
 
select 
 
    "NAME" as u_name,
    "PHONE_NO" as u_phone_no,
    "CITY" as u_city,
    "DOR" as u_dor,
    "DOB" as u_dob,
    "STATE" as u_state,
    "EMAIL" as u_email,
    "GENDER" as u_gender,
    "CUSTOMER_ID" as cust_id,
    "AGE_IN_YEARS_" as age,
    "DEALERSHIP_ID" as dealership_id,
    "HOW_THEY_FOUND_US_" as how_they_found_us

 
from source 
 
) 
 
select * from user_renamed