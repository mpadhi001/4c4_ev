with source as (
 
    select * from {{source('Snowflake_Landing','RDS_ORDERS_DATA') }}
 
),

orders_renamed as (
 
select 
 
    "MODEL_NUM" as model_no,
    "DELIVERY_DATE" as del_date,
    "ORDER_ID" as order_id,
    "CUSTOMER_ID" as cust_id,
    "DEALERSHIP_ID" as deal_id,
    "QUANTITY" as quantity,
    "EXP_DELIVERY_DATE" as exp_date,
    "MODE_OF_PAYMENT" as mo_payment,
    "DATE_OF_PURCHASE" as do_purchase
 
from source 
 
) 
 
select * from orders_renamed