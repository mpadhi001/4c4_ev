with source as (
 
    select * from {{source('Snowflake_Landing','COMPETITORS_DATA') }}
 
), 
 
competitor_renamed as (
 
select 
 
    "COMPANY" as company_name,
    "JANUARY" as jan_21,
    "FEBRUARY" as feb_21,
    "MARCH" as march_21,
    "APRIL" as april_21,
    "MAY" as may_21,
    "JUNE" as june_21,
    "JULY" as july_21,
    "AUGUST" as aug_21,
    "SEPTEMBER" as sep_21,
    "OCTOBER" as oct_21,
    "NOVEMBER" as nov_21,
    "DECEMBER" as dec_21,	
    "JAN_2022" as jan_22,	
    "TOTAL" as total_sales,	
    "MARKET_PERCENTAGE_SHARE" as market_share

 
from source 
 
) 
 
select * from competitor_renamed