with source as (
 
    select * from {{source('Snowflake_Landing','B2B_DATA') }}
 
), 
 
b2b_renamed as (
 
select 
 
    "PRODUCT" as product,
    "DRIVER" as driver,
    "DRIVE_RANGE" as drive_range,
    "ISSUE" as issue,
    "ATTACHMENT" as attachment,
    "LOCATION" as loc

 
from source 
 
) 
 
select * from b2b_renamed